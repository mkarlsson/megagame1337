* Disabling VAOs and Attribs can make things not render in renderdoc
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindVertexArray(0);
* shaders should be stored in a manager
* textures should also be stored in manager
* VAOs are basically layouts and you can do everything with one VAO that is always
bound and use bound buffer data and layout with attribpointers
* make some manager that holds tiles and all colliders
* store tiles as single frame animations to make multi frame possible ?
* fix simple collisions - done
* fix grappling hook. both directions, force after release, add recoil force
