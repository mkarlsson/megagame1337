#include <stdio.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <cglm/cglm.h>
#include <string.h>
#include <unistd.h>
#include <cglm/call/io.h>
#include "external/cJSON/cJSON.h"
#include "external/SOIL2/src/SOIL2/SOIL2.h"
int WIDTH = 1920;
int HEIGHT = 1080;


vec3 camera_pos = { 16.0f, -10.0f, 24.0f};
vec3 camera_front = { 0.0f, 0.0f, -1.0f};
vec3 camera_up = { 0.0f, 1.0f, 0.0f};
vec3 hook_ancor = {0.0f,0.0f,0.0f};
vec3 hook_perp = {0.0f,0.0f,0.0f};
float hook_radius = 0.0f;
float hook_circumference = 0.0f;
bool hooked = false;
bool keyReleased = true;
struct Sprite megaman;
struct Sprite tiles;
float jump_force = 12.5f;
float speed = 6.5f;
float recoil = 17.0f;
GLuint shader;
GLuint collider_shader;
GLuint line_shader;
bool printMatrix = false;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS){
        double xpos, ypos;
        //getting cursor position
        glfwGetCursorPos(window, &xpos, &ypos);
        printf("Cursor Position at x: %f y:%f\n",xpos,ypos);

    }
}

mat4 view = GLM_MAT4_IDENTITY_INIT; // init to identity
mat4 projection = GLM_MAT4_IDENTITY_INIT; // init to identity
mat4 viewProjection = GLM_MAT4_IDENTITY_INIT;
mat4 inverseProjection = GLM_MAT4_IDENTITY_INIT;
GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);
void renderLine(vec3 *points,int numberOfPoints,vec3 *color);

unsigned int TextureFromFile(const char *filen, const char* directory, bool gamma, bool pixelated);
struct SpriteAnimationFrame{
    float vertices[20];
    unsigned int indecies[6];
};
struct SpriteAnimation{
    char name[100];
    uint8_t number_of_frames;
    uint8_t current_frame;
    float accumulated_time;
    uint8_t frame_array[20];
    float frame_time_sec;
};
struct Sprite{
    //vec4 for hit test
    vec3 collision_box[4];
    bool hasCollider;
    bool colliding;
    bool grounded;
    int direction;
    vec3 position;
    vec3 force;
    GLuint VAO, VBO, EBO, CVAO, CVBO;
    unsigned int texture;
    uint8_t num_of_animations;
    uint8_t current_animation;
    mat4 model;
    struct SpriteAnimation animations[10];
    struct SpriteAnimationFrame frames[40];
};
struct Tile{
    struct Sprite sprite;
    vec3 position;
    bool colliding;
    unsigned int animation;
    unsigned int frame;
};
struct TileManager{
    struct Tile tiles[100];
    unsigned int number_of_tiles;
};

void checkCollisions(struct Sprite *player, struct TileManager *tm);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void renderSprite(struct Sprite *sprite, float delta_time);
// TODO fix this shit
void renderCollider(struct Sprite *sprite, float delta_time, bool collided);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    float ndc_x, ndc_y = 0.0f;
    double xpos, ypos;
    //getting cursor position
    glfwGetCursorPos(window, &xpos, &ypos);
    printf("Cursor Position at x: %f y:%f\n ", xpos, ypos);
    ndc_x = (2.0f*(xpos/WIDTH))-1.0f;
    ndc_y = 1.0f-(2.0f*(ypos/HEIGHT));
    float ndc_z = 0.8694f;
    vec4 ndc_v4 = {ndc_x,ndc_y,ndc_z,1.0f};
    printf("NDC x: %f y: %f \n",ndc_x,ndc_y);
    vec4 ass;
    glm_mat4_mulv((vec4 *) &inverseProjection, (float *) &ndc_v4, (float *) &ass);
    printf("After invVec mul x: %f y: %f \n",ass[0],ass[1]);
    ass[3] = 1.0f/ass[3];
    ass[0] *= ass[3];
    ass[1] *= ass[3];
    ass[2] *= ass[3];
    printf("After perspdiv mul x: %f y: %f \n",ass[0],ass[1]);
    printf("==========================================================\n");
    /*    printf("View Matix \n");
        glmc_mat4_print(&view,stdout);
        printf("Proj Matix \n");
        glmc_mat4_print(&projection,stdout);
        printf("ViewProj Matix \n");
        glmc_mat4_print(&viewProjection,stdout);*/
    // printf("NDC Position at x: %f y:%f\n ", fartResult[0], fartResult[1]);
    // printf("Pick Position at x: %f y:%f\n ", dest[0], dest[1]);
    printf("Player Position at x: %f y:%f\n ", megaman.position[0], megaman.position[1]);
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {

        //printMatrix = true;
        vec3 force,perp;
        ass[2] = 0.0f;
        hook_ancor[0] = ass[0];
        hook_ancor[1] = ass[1];
        hooked = true;
        vec3 zero = {0.0f,0.0f,0.0f};
        hook_radius = glm_vec3_distance(&hook_ancor, &megaman.position);
        hook_circumference = 2 * CGLM_PI * hook_radius;
        printf("\nHook radius: %f circ: %f\n", hook_radius, hook_circumference);
        //glm_vec3_sub((float *) &ass, (float *) &megaman.position, (float *) &megaman.force);
        glm_vec3_sub((float *) &hook_ancor, (float *) &megaman.position, (float *) &hook_perp);
        vec3 axis = {0.0f,0.0f,-1.0f};
        if(megaman.force[0] > 0.0f){
            glm_vec3_rotate(&hook_perp, glm_rad(90),axis);
        }else{
            glm_vec3_rotate(&hook_perp, glm_rad(-90),axis);
        }
        glm_vec3_add(&megaman.position,&hook_perp,&hook_perp);
        //glm_vec3_scale((float *) &megaman.force, 5.0f, (float *) &megaman.force);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE){
        hooked = false;
    }
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        vec3 force;
        ass[2] = 0.0f;
        glm_vec3_sub((float *) &ass, (float *) &megaman.position, (float *) &force);
        glm_vec3_inv(force);
        glm_vec3_normalize(force);
        glm_vec3_scale(force,recoil,force);
        glm_vec3_add(&force,&megaman.force,megaman.force);
    }
}
int main() {
    mat4 origo = GLM_MAT4_IDENTITY_INIT;
    char * buffer = 0;
    long length;
    cJSON *json;
    FILE * f = fopen ("../resources/textures/sprites/sprites.json", "rb");
    if (f)
    {
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        buffer = malloc (length);
        if (buffer)
        {
            fread (buffer, 1, length, f);
        }
        fclose (f);
    }

    if (buffer)
    {

        json = cJSON_Parse(buffer);
        free(buffer);
        // start to process your data / extract strings here...
    }

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "megaspel", NULL, NULL);
    if (window == NULL)
    {
        printf("Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        printf("Failed to initialize GLAD\n");
        return -1;
    }
    shader = LoadShaders("../shaders/TextureShader.vert",
                                "../shaders/TextureShader.frag");
    collider_shader = LoadShaders("../shaders/Collider.vert",
                                "../shaders/Collider.frag");
    line_shader = LoadShaders("../shaders/Line.vert",
                              "../shaders/Collider.frag");

    unsigned int indices[] = {
            0, 1, 3, // first triangle
            1, 2, 3  // second triangle
    };
    //int number_of_frames = 5;
    //int current_frame = 2;
    glEnable(GL_DEPTH_TEST);
    // load and create a texture
    // -------------------------
    unsigned int texture = TextureFromFile("Megatile.tga","../resources/textures/",false, true);
    unsigned int tileset = TextureFromFile("walls.tga","../resources/textures/tiles/MysteryForrest/Walls/",false,true);
    unsigned int background_texture = TextureFromFile("bck.tga","../resources/textures/",false, false);
   // mat4 bck_model = GLM_MAT4_IDENTITY_INIT;
    //mat4 model = GLM_MAT4_IDENTITY_INIT;// init to identity
   // mat4 view = GLM_MAT4_IDENTITY_INIT; // init to identity
   // mat4 projection = GLM_MAT4_IDENTITY_INIT; // init to identity
    vec3 camera_center = {0.0f, 0.0f, 0.0f};
    glm_vec3_add((float *) &camera_pos, (float *) &camera_front, (float *) &camera_center);
    glm_lookat(camera_pos,
               camera_center,
               camera_up,
               (vec4 *) &view);
    glm_perspective(glm_rad(45.0f), (float)WIDTH/(float) HEIGHT, 2.0f, 100.0f, (vec4 *) &projection);
    glm_mat4_mul(projection,view , viewProjection);
    glm_mat4_inv_fast((vec4 *) &viewProjection, (vec4 *) &inverseProjection);
    struct Sprite background;
    background.texture = background_texture;
    memcpy(background.model, origo, sizeof(mat4));
    background.direction = 1;
    background.hasCollider = false;
    background.position[0] = 0.0f;
    background.position[1] = 0.0f;
    background.position[2] = -2.0f;
    background.force[0] = 0.0f;
    background.force[1] = 0.0f;
    background.force[2] = 0.0f;
    background.num_of_animations = 1;
    background.current_animation = 0;
    background.animations[0].number_of_frames = 1;
    background.animations[0].current_frame = 0;
    background.animations[0].accumulated_time = 0;
    background.animations[0].frame_time_sec = 100.0f;
    background.animations[0].frame_array[0] = 0;
    float verts[20] = {
            // positions       // UVs
            10.5f,  10.5f, 0.0f,  1.0f, 1.0f,
            10.5f, -10.5f, 0.0f,  1.0f, 0.0f,
            -10.5f, -10.5f, 0.0f, 0.0f, 0.0f,
            -10.5f,  10.5f, 0.0f, 0.0f, 1.0f
    };
    memcpy(background.frames[0].vertices ,verts,sizeof(verts));
    memcpy(background.frames[0].indecies,indices,sizeof(indices));
    background.collision_box[0][0] = 0.5f;
    background.collision_box[0][1] = 0.5f;
    background.collision_box[0][2] = 0.0f;

    background.collision_box[1][0] = 0.5f;
    background.collision_box[1][1] = -0.5f;
    background.collision_box[1][2] = 0.0f;

    background.collision_box[2][0] = -0.5f;
    background.collision_box[2][1] = -0.5f;
    background.collision_box[2][2] = 0.0f;

    background.collision_box[3][0] = -0.5f;
    background.collision_box[3][1] = 0.5f;
    background.collision_box[3][2] = 0.0f;


    tiles.texture = tileset;
    tiles.hasCollider = true;
    tiles.position[0] = 0.0f;
    tiles.position[1] = 0.0f;
    tiles.position[2] = 0.0f;
    tiles.force[0] = 0.0f;
    tiles.force[1] = 0.0f;
    tiles.force[2] = 0.0f;
    tiles.direction = 1;
    tiles.num_of_animations = 1;
    tiles.current_animation = 0;
    tiles.animations[0].number_of_frames = 20;
    tiles.animations[0].current_frame = 1;
    tiles.animations[0].accumulated_time = 0;
    tiles.animations[0].frame_time_sec = 100.0f;
    tiles.animations[0].frame_array[0] = 0;
    tiles.animations[0].frame_array[1] = 1;
    tiles.animations[0].frame_array[2] = 2;
    tiles.animations[0].frame_array[3] = 3;
    tiles.animations[0].frame_array[4] = 4;
    tiles.animations[0].frame_array[5] = 5;
    tiles.animations[0].frame_array[6] = 6;
    tiles.animations[0].frame_array[7] = 7;
    tiles.animations[0].frame_array[8] = 8;
    tiles.animations[0].frame_array[9] = 9;
    tiles.animations[0].frame_array[10] = 10;
    tiles.animations[0].frame_array[11] = 11;
    tiles.animations[0].frame_array[12] = 12;
    tiles.animations[0].frame_array[13] = 13;
    tiles.animations[0].frame_array[14] = 14;
    tiles.animations[0].frame_array[15] = 15;
    tiles.animations[0].frame_array[16] = 16;
    tiles.animations[0].frame_array[17] = 17;
    tiles.animations[0].frame_array[18] = 18;
    tiles.animations[0].frame_array[19] = 19;
    int frame = 0;
    int col = 5;
    int row = 4;

    for(int i = 0;i < tiles.animations[0].number_of_frames; i++){
        frame = i;
        float frameWidth  = 1.0f/col;
        float frameHeight  = 1.0f/row;

        int coln = frame;
        int rown = 0;
        if(frame >= col){
            rown = frame/col;
            coln = frame%col;
        }
        int numFrames = megaman.animations[0].number_of_frames;
        float verts[20] = {
                // positions       // UVs
                0.5f,  0.5f, 0.0f,  frameWidth + (frameWidth * coln), frameHeight + (frameHeight * rown),  //top right
                0.5f, -0.5f, 0.0f,  frameWidth + (frameWidth * coln), frameHeight * rown,  //bottom right
                -0.5f, -0.5f, 0.0f, frameWidth * coln, frameHeight * rown,    // bottom left
                -0.5f,  0.5f, 0.0f, frameWidth * coln, frameHeight + (frameHeight * rown)    // top left
        };
        memcpy(tiles.frames[i].vertices ,verts,sizeof(verts));
        memcpy(tiles.frames[i].indecies,indices,sizeof(indices));
    }
    memcpy(tiles.model, origo, sizeof(mat4));
    tiles.collision_box[0][0] = 0.5f;
    tiles.collision_box[0][1] = 0.5f;
    tiles.collision_box[0][2] = 0.0f;

    tiles.collision_box[1][0] = 0.5f;
    tiles.collision_box[1][1] = -0.5f;
    tiles.collision_box[1][2] = 0.0f;

    tiles.collision_box[2][0] = -0.5f;
    tiles.collision_box[2][1] = -0.5f;
    tiles.collision_box[2][2] = 0.0f;

    tiles.collision_box[3][0] = -0.5f;
    tiles.collision_box[3][1] = 0.5f;
    tiles.collision_box[3][2] = 0.0f;

    megaman.texture = texture;

    memcpy(megaman.model, origo, sizeof(mat4));
    megaman.hasCollider = true;
    megaman.direction = 1;
    megaman.position[0] = 0.0f;
    megaman.position[1] = 0.0f;
    megaman.position[2] = 0.0f;
    megaman.force[0] = 0.0f;
    megaman.force[1] = 0.0f;
    megaman.force[2] = 0.0f;
    megaman.num_of_animations = 1;
    megaman.current_animation = 0;
    megaman.animations[0].number_of_frames = 14;
    megaman.animations[0].current_frame = 0;
    megaman.animations[0].accumulated_time = 0;
    megaman.animations[0].frame_time_sec = 0.03f;
    megaman.animations[0].frame_array[0] = 0;
    megaman.animations[0].frame_array[1] = 1;
    megaman.animations[0].frame_array[2] = 2;
    megaman.animations[0].frame_array[3] = 3;
    megaman.animations[0].frame_array[4] = 4;
    megaman.animations[0].frame_array[5] = 5;
    megaman.animations[0].frame_array[6] = 6;
    megaman.animations[0].frame_array[7] = 7;
    megaman.animations[0].frame_array[8] = 8;
    megaman.animations[0].frame_array[9] = 9;
    megaman.animations[0].frame_array[10] = 10;
    megaman.animations[0].frame_array[11] = 11;
    megaman.animations[0].frame_array[12] = 12;
    megaman.animations[0].frame_array[13] = 13;

    for(int i = 0;i < megaman.animations[0].number_of_frames; i++){
        int numFrames = megaman.animations[0].number_of_frames;
        float verts[20] = {
                // positions       // UVs
                0.5f,  0.5f, 0.0f,  1.0f/numFrames*(i+1), 1.0f,
                0.5f, -0.5f, 0.0f,  1.0f/numFrames*(i+1), 0.0f,
                -0.5f, -0.5f, 0.0f, 1.0f/numFrames*(i), 0.0f,
                -0.5f,  0.5f, 0.0f, 1.0f/numFrames*(i), 1.0f
        };
        memcpy(megaman.frames[i].vertices ,verts,sizeof(verts));
        memcpy(megaman.frames[i].indecies,indices,sizeof(indices));
    }
    megaman.collision_box[0][0] = 0.5f;
    megaman.collision_box[0][1] = 0.5f;
    megaman.collision_box[0][2] = 0.0f;

    megaman.collision_box[1][0] = 0.5f;
    megaman.collision_box[1][1] = -0.5f;
    megaman.collision_box[1][2] = 0.0f;

    megaman.collision_box[2][0] = -0.5f;
    megaman.collision_box[2][1] = -0.5f;
    megaman.collision_box[2][2] = 0.0f;

    megaman.collision_box[3][0] = -0.5f;
    megaman.collision_box[3][1] = 0.5f;
    megaman.collision_box[3][2] = 0.0f;

    strcpy(megaman.animations[0].name, "assimation");
    vec3 tile_positions[5] = {
            {-1.5f,  -3.0f,  0.0f},
            {-0.5f,  -3.0f,  0.0f},
            {0.5f,  -3.0f,  0.0f},
            {1.5f,  -3.0f,  0.0f},
            {2.5f,  -3.0f,  0.0f},
    };

    // TODO: move into sprite
    unsigned int HBO;
    glGenBuffers(1,&HBO);
    glGenVertexArrays(1, &megaman.VAO);
    //lGenBuffers(1, &megaman.VAO);
    glGenBuffers(1, &megaman.VBO);
    glGenBuffers(1, &megaman.CVBO);
    glGenBuffers(1, &megaman.EBO);

    glBindVertexArray(megaman.VAO);

    glBindBuffer(GL_ARRAY_BUFFER, megaman.VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(megaman.frames[0].vertices), megaman.frames[0].vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, megaman.EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(megaman.frames[0].indecies), megaman.frames[0].indecies, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);

    glGenVertexArrays(1, &background.VAO);
    glGenBuffers(1, &background.VBO);

    glGenBuffers(1, &background.EBO);

    glBindVertexArray(background.VAO);

    glBindBuffer(GL_ARRAY_BUFFER, background.VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(background.frames[0].vertices), background.frames[0].vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, background.EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(background.frames[0].indecies), background.frames[0].indecies, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    //glBindVertexArray(0);

    glGenVertexArrays(1, &tiles.VAO);
    glGenVertexArrays(1, &tiles.CVAO);
    glGenBuffers(1, &tiles.VBO);
    glGenBuffers(1, &tiles.CVBO);
    glGenBuffers(1, &tiles.EBO);

    glBindVertexArray(tiles.VAO);

    glBindBuffer(GL_ARRAY_BUFFER, tiles.VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(tiles.frames[0].vertices), tiles.frames[0].vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tiles.EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(tiles.frames[0].indecies), tiles.frames[0].indecies, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    //glDisableVertexAttribArray(0);
    //glDisableVertexAttribArray(1);
    //glBindVertexArray(0);
    // color attribute
    //glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    //glEnableVertexAttribArray(1);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    // render loop
    // -----------
    float deltaTime,lastFrame;
    glLineWidth(3.3f);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    struct TileManager tm;
    for(int i = 0; i < 5;i++){
        struct Tile t;
        t.sprite = tiles;
        memcpy(&t.position,&tile_positions[i],sizeof(vec3));
        t.animation = 0;
        t.frame = i;
        tm.tiles[i] = t;
    }
    tm.number_of_tiles = 5;
    while (!glfwWindowShouldClose(window))
    {

        float currentFrame = (float)glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;


        // Ugly gravity
        float gravity = 0.6f;
        if(!hooked) {

            if (!megaman.grounded) {
                megaman.force[1] -= gravity;
            }
            if (megaman.grounded && megaman.force[1] < 0.0f) {
                megaman.force[1] = 0.0f;
                //megaman.position[1] = -2.1f;
            }
            if (megaman.position[1] < -61.0f) {
                megaman.position[0] = 0.0f;
                megaman.position[1] = 0.0f;
                megaman.force[0] = 0.0f;
                megaman.force[1] = 0.0f;
                megaman.force[2] = 0.0f;

            }
            vec3 scaled_force;
            glm_vec3_scale(megaman.force,deltaTime,scaled_force);
            glm_vec3_add(megaman.position,scaled_force ,megaman.position);
        }else{
            //megaman.force[1] -= gravity;
            glm_vec3_sub((float *) &hook_ancor, (float *) &megaman.position, (float *) &hook_perp);
            vec3 axis = {0.0f,0.0f,-1.0f};
            //if(megaman.force[0] > 0.0f){
                glm_vec3_rotate(&hook_perp, glm_rad(90),axis);
            //}
            /*else{
                glm_vec3_rotate(&hook_perp, glm_rad(-90),axis);
            }*/
            glm_vec3_add(&megaman.position,&hook_perp,&hook_perp);

            vec3 rotation_vector = {hook_perp[0], hook_perp[1], hook_perp[2]};
            vec3 zero = {0.0f,0.0f,0.0f};
            float distance = glm_vec3_distance(zero, megaman.force);
            glm_vec3_normalize(&rotation_vector);
            glm_vec3_scale(&rotation_vector,distance,&rotation_vector);
            megaman.force[0] = rotation_vector[0];
            megaman.force[1] = rotation_vector[1];
            megaman.force[2] = rotation_vector[2];
            vec3 scaled_force;
            glm_vec3_scale(megaman.force,deltaTime,scaled_force);
            float scaled_distance = glm_vec3_distance(zero, scaled_force);
            //glm_vec3_add(megaman.position,scaled_force ,megaman.position);
            float scaled_degrees = (scaled_distance / hook_circumference);
            // find make and scale vector between anchor and player
            vec3 rotation_pos_vec;
            glm_vec3_sub( (float *) &megaman.position, &hook_ancor, (float *) &rotation_pos_vec);
            glm_vec3_normalize(rotation_pos_vec);
            glm_vec3_scale(&rotation_pos_vec,hook_radius,&rotation_pos_vec);
            glm_vec3_rotate(&rotation_pos_vec, glm_rad(-scaled_degrees*360),axis);
            glm_vec3_add(&hook_ancor,&rotation_pos_vec,&rotation_pos_vec);
            megaman.position[0] = rotation_pos_vec[0];
            megaman.position[1] = rotation_pos_vec[1];
            //float hook_angle = glm_deg(glm_vec3_angle(&hook_ancor, &megaman.position));
            printf("Scaled distance %f Degrees: %f megaman :x %f :y %f\n",scaled_distance, scaled_degrees,megaman.position[0],megaman.position[1]);
        }


        if(keyReleased) {
         //   megaman.force[0] *= 0.98f;
        }

        checkCollisions(&megaman,&tm);
        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // bind Texture
        //glBindTexture(GL_TEXTURE_2D, megaman.texture);

        // render container


        // probably not needed to bind uniforms everyframe

        //glUniform1i(glGetUniformLocation(shader, "numberOfFrames"), number_of_frames);
        //glUniform1i(glGetUniformLocation(shader, "currentFrame"), current_frame);
        /*glBindVertexArray(megaman.VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);*/
        glUseProgram(shader);
        //glUniformMatrix4fv(glGetUniformLocation(shader,"model"), 1, GL_FALSE, (const GLfloat *) &model);
        glUniformMatrix4fv(glGetUniformLocation(shader,"projection"), 1, GL_FALSE, (const GLfloat *) &projection);
        glUniformMatrix4fv(glGetUniformLocation(shader,"view"), 1, GL_FALSE, (const GLfloat *) &view);
        glm_translate_to((vec4 *) &origo, (float *) background.position, (vec4 *) &background.model);
        renderSprite(&background,0.0f);
        glm_translate_to((vec4 *) &origo, (float *) megaman.position, (vec4 *) &megaman.model);
        renderSprite(&megaman,deltaTime);
        glm_translate_to((vec4 *) &origo, (float *) tiles.position, (vec4 *) &tiles.model);
        //renderSprite(&tiles,0.0f);
        for(int i = 0; i< tm.number_of_tiles; i++){
            glm_translate_to((vec4 *) &origo, (float *) tm.tiles[i].position, (vec4 *) &tm.tiles[i].sprite.model);
            tm.tiles[i].sprite.animations[tm.tiles[i].animation].current_frame = tm.tiles[i].frame;
            renderSprite(&tm.tiles[i].sprite,0.0f);
        }
        /*if(printMatrix){
            glmc_mat4_print(&view,stdout);
            glmc_mat4_print(&projection,stdout);
            glmc_mat4_print(&viewProjection,stdout);
            printMatrix = false;
        }*/
        //glm_translate_to((vec4 *) &origo, (float *) &background.position,(vec4 *) &model);
        /*glBindVertexArray(tiles.VAO);
        for(int i = 0; i<5; i++) {
            mat4 tile_model = GLM_MAT4_IDENTITY_INIT;
            glm_translate((vec4 *) &tile_model, tile_positions[i]);
            tiles.animations[0].current_frame = i;
            glUniform1i(glGetUniformLocation(shader,"direction"),(GLint) background.direction);
            glUniformMatrix4fv(glGetUniformLocation(shader,"model"), 1, GL_FALSE, (const GLfloat *) &tile_model);
            renderSprite(&tiles, 0.0f,true);
        }

        glUniformMatrix4fv(glGetUniformLocation(shader,"model"), 1, GL_FALSE, (const GLfloat *) &bck_model);
        glUniform1i(glGetUniformLocation(shader,"direction"),(GLint) background.direction);
        renderSprite(&background,deltaTime,false);
        // draw collition box.*/
        glUseProgram(collider_shader);
    //    glm_translate_to((vec4 *) &origo, (float *) &megaman.position,(vec4 *) &megaman.model);
  //      glUniformMatrix4fv(glGetUniformLocation(collider_shader,"model"), 1, GL_FALSE, (const GLfloat *) &megaman.model);
        glUniformMatrix4fv(glGetUniformLocation(collider_shader,"projection"), 1, GL_FALSE, (const GLfloat *) &projection);
        glUniformMatrix4fv(glGetUniformLocation(collider_shader,"view"), 1, GL_FALSE, (const GLfloat *) &view);

        renderCollider(&megaman,deltaTime, megaman.colliding);
        if(hooked) {
            vec3 hook_vertex[2] = {
                    {hook_ancor[0],  hook_ancor[1],  hook_ancor[2]},
                    {megaman.position[0],  megaman.position[1],  megaman.position[2]}
            };
            vec3 hook_perp_vertex[2] = {
                    {hook_perp[0],  hook_perp[1],  hook_perp[2]},
                    {megaman.position[0],  megaman.position[1],  megaman.position[2]}
            };
            mat4 origo = GLM_MAT4_IDENTITY_INIT;
            glUseProgram(line_shader);
            glUniformMatrix4fv(glGetUniformLocation(collider_shader,"projection"), 1, GL_FALSE, (const GLfloat *) &projection);
            glUniformMatrix4fv(glGetUniformLocation(collider_shader,"view"), 1, GL_FALSE, (const GLfloat *) &view);
            glUniformMatrix4fv(glGetUniformLocation(line_shader,"model"), 1, GL_FALSE, (const GLfloat *) &origo);
            vec3 color = {0.0f,0.0f,1.0f};
            glBindBuffer(GL_ARRAY_BUFFER, HBO);
            //glUniform3fv(glGetUniformLocation(line_shader, "collided"), 1, (const GLfloat *) &color);
            renderLine((vec3 *) &hook_vertex, 2, &color);
            vec3 perp_color = {0.0f,1.0f,1.0f};
            glBindBuffer(GL_ARRAY_BUFFER, HBO);
            renderLine((vec3 *) &hook_perp_vertex, 2, &perp_color);
            vec3 force_vertex[2] ={
                    {megaman.force[0], megaman.force[1], megaman.force[2]},
                    {megaman.position[0],  megaman.position[1],  megaman.position[2]}
            };
            glBindBuffer(GL_ARRAY_BUFFER, HBO);
            vec3 force_color = {1.0f,0.0f,0.0f};
            renderLine((vec3 *) &force_vertex, 2, &force_color);
           // glBindVertexArray(megaman.VAO);
            /*glBindBuffer(GL_ARRAY_BUFFER, HBO);
            glBufferData(GL_ARRAY_BUFFER,6 * sizeof(float), &hook_vertex,GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            glDepthFunc(GL_ALWAYS);
            glDrawArrays(GL_LINE_LOOP,0,2);
            glDepthFunc(GL_LESS);*//*
            glUniform1i(glGetUniformLocation(collider_shader, "collided"), true);
            glBufferData(GL_ARRAY_BUFFER,6 * sizeof(float), &hook_perp_vertex,GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            glDepthFunc(GL_ALWAYS);
            glDrawArrays(GL_LINE_LOOP,0,2);
            glDepthFunc(GL_LESS);*/

        }
        glUseProgram(collider_shader);
        //renderCollider(&tiles,0.0f, true);
        for(int i = 0; i< tm.number_of_tiles; i++){
            glm_translate_to((vec4 *) &origo, (float *) tm.tiles[i].position, (vec4 *) &tm.tiles[i].sprite.model);
            tm.tiles[i].sprite.animations[tm.tiles[i].animation].current_frame = tm.tiles[i].frame;
            renderCollider(&tm.tiles[i].sprite,0.0f,tm.tiles[i].colliding);
        }

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &megaman.VAO);
    glDeleteBuffers(1, &megaman.VBO);
    glDeleteBuffers(1, &megaman.EBO);

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}
void renderLine(vec3 *points,int numberOfPoints,vec3 *color){
    mat4 origo = GLM_MAT4_IDENTITY_INIT;
    glUniformMatrix4fv(glGetUniformLocation(line_shader,"model"), 1, GL_FALSE, (const GLfloat *) &origo);
    glUniform3fv(glGetUniformLocation(line_shader, "color"), 1, (const GLfloat *) *color);
    // glBindVertexArray(megaman.VAO);
    //glBindBuffer(GL_ARRAY_BUFFER, VAO);
    glBufferData(GL_ARRAY_BUFFER, 3 *numberOfPoints * sizeof(float), points,GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
    //glEnableVertexAttribArray(0);
    glDepthFunc(GL_ALWAYS);
    glDrawArrays(GL_LINE_LOOP,0,numberOfPoints);
    glDepthFunc(GL_LESS);
}
void renderSprite(struct Sprite *sprite, float delta_time){
    // TODO: shorten this shit
    mat4 origo = GLM_MAT4_IDENTITY_INIT;
    glUniform1i(glGetUniformLocation(shader,"direction"),(GLint) sprite->direction);
    // moved out glm_translate_to((vec4 *) &origo, (float *) sprite->position, (vec4 *) &sprite->model);
    glUniformMatrix4fv(glGetUniformLocation(shader,"model"), 1, GL_FALSE, (const GLfloat *) &sprite->model);
    sprite->animations[sprite->current_animation].accumulated_time = sprite->animations[sprite->current_animation].accumulated_time + delta_time;
    if(sprite->animations[sprite->current_animation].accumulated_time > sprite->animations[sprite->current_animation].frame_time_sec){
        if(sprite->animations[sprite->current_animation].current_frame != sprite->animations[sprite->current_animation].number_of_frames-1) {
            sprite->animations[sprite->current_animation].current_frame = sprite->animations[sprite->current_animation].current_frame +1;
        }else {
            sprite->animations[sprite->current_animation].current_frame = 0;
        }
        sprite->animations[sprite->current_animation].accumulated_time = 0.0f;
    }


    glBindVertexArray(sprite->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, sprite->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sprite->frames[sprite->animations[0].frame_array[sprite->animations[0].current_frame]].vertices),
               sprite->frames[sprite->animations[0].frame_array[sprite->animations[0].current_frame]].vertices, GL_STATIC_DRAW);

    /*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sprite->frames[sprite->animations[0].frame_array[sprite->animations[0].current_frame]].vertices),
                 sprite->frames[sprite->animations[0].frame_array[sprite->animations[0].current_frame]].indecies, GL_STATIC_DRAW);

    // position attribute*/

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glBindTexture(GL_TEXTURE_2D, sprite->texture);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

};
void renderCollider(struct Sprite *sprite, float delta_time,bool collided){
    mat4 origo = GLM_MAT4_IDENTITY_INIT;

    glUniformMatrix4fv(glGetUniformLocation(collider_shader,"model"), 1, GL_FALSE, (const GLfloat *) &sprite->model);
    glUniform1i(glGetUniformLocation(collider_shader, "collided"), collided);
    glBindVertexArray(sprite->VAO);
    /*for(int i = 0; i < 4; i++) {
        sprite->collision_box[i][3] = collided;
    }*/
    // graphicscard buffer.
    glBindBuffer(GL_ARRAY_BUFFER, sprite->CVBO);
    glBufferData(GL_ARRAY_BUFFER,16 * sizeof(float), sprite->collision_box,GL_STATIC_DRAW);
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
    //glVertexAttribPointer(1, 1, GL_BOOL, GL_FALSE, 3*sizeof(float), (void*)0);

    glEnableVertexAttribArray(0);
    glDepthFunc(GL_ALWAYS);
    glDrawArrays(GL_LINE_LOOP,0,4);
    glDepthFunc(GL_LESS);
}
GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path){

    const char *vert_shader_src;
    const char *frag_shader_src;

    long input_file_size;
    FILE *f = fopen(vertex_file_path,"r");
    fseek(f, 0, SEEK_END);
    input_file_size = ftell(f) + 1;
    rewind(f);
    vert_shader_src = malloc(sizeof(char) * input_file_size);
    memset((char *)vert_shader_src,'\0',(size_t)input_file_size);
    fread((char *)vert_shader_src, sizeof(char),(size_t)input_file_size,f);
    printf("%s", vert_shader_src);
    fclose(f);

    f = fopen(fragment_file_path,"r");
    fseek(f, 0, SEEK_END);
    input_file_size = ftell(f) + 1;
    rewind(f);
    frag_shader_src = malloc(sizeof(char) * input_file_size);
    memset((char *)frag_shader_src,'\0',(size_t)input_file_size);//make valgrind happy,terminate '/0'
    fread((char *)frag_shader_src, sizeof(char),(size_t)input_file_size,f);
    printf("%s",frag_shader_src);
    fclose(f);

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1,  &vert_shader_src, NULL);
    glCompileShader(vertexShader);
    free((char *)vert_shader_src);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        printf("%s",infoLog);
    }
    // fragment shader
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &frag_shader_src, NULL);
    glCompileShader(fragmentShader);
    free((char *)frag_shader_src);
    // check for shader compile errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        printf("%s",infoLog);
    }
    // link shaders
    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        printf("%s",infoLog);
    }
    return shaderProgram;
}
void checkCollisions(struct Sprite *player, struct TileManager *tm){

    player->colliding = false;
    player->grounded = false;
    for(int i = 0;i < tm->number_of_tiles; i++){
        vec3 world_pos_tr1,world_pos_br1,world_pos_tl1,world_pos_bl1,
            world_pos_tr2,world_pos_br2,world_pos_tl2,world_pos_bl2;
        glm_vec3_add(tm->tiles[i].position, tm->tiles[i].sprite.collision_box[0], (float *) &world_pos_tr1);
        glm_vec3_add(tm->tiles[i].position, tm->tiles[i].sprite.collision_box[1], (float *) &world_pos_br1);
        glm_vec3_add(tm->tiles[i].position, tm->tiles[i].sprite.collision_box[2], (float *) &world_pos_bl1);
        glm_vec3_add(tm->tiles[i].position, tm->tiles[i].sprite.collision_box[3], (float *) &world_pos_tl1);
        glm_vec3_add(player->position, player->collision_box[0], (float *) &world_pos_tr2);
        glm_vec3_add(player->position, player->collision_box[1], (float *) &world_pos_br2);
        glm_vec3_add(player->position, player->collision_box[2], (float *) &world_pos_bl2);
        glm_vec3_add(player->position, player->collision_box[3], (float *) &world_pos_tl2);
        //  bottom1 <= top2    && top1    >= bottom2 &&   left1 <= right2  && right1  >= left2
        if(world_pos_br1[1] <= world_pos_tr2[1] && world_pos_tr1[1] >=  world_pos_br2[1] &&
           world_pos_bl1[0] <= world_pos_br2[0] && world_pos_br1[0] >= world_pos_bl2[0] ){
            player->colliding = true;
            tm->tiles[i].colliding = true;
            // top of tile - 0.2    > bottom of player
            if((world_pos_tr1[1]-0.2 >= world_pos_br2[1] || world_pos_tr1[1] == world_pos_br2[1])
                && !(world_pos_br2[1] < world_pos_tr1[1]-0.5)) {
                player->position[1] = world_pos_tr1[1] + 0.5f;
                player->grounded = true;
            }
            //}
        }
        else{
            tm->tiles[i].colliding = false;
        }

    }

}
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS){
        double xpos, ypos;
        //getting cursor position
        glfwGetCursorPos(window, &xpos, &ypos);
        printf("Cursor Position at x: %f y:%f\n",xpos,ypos);

    }
    if (key == GLFW_KEY_A && action == GLFW_PRESS){
        megaman.force[0] = -speed;
        megaman.direction = -1;
        keyReleased = false;
    }
    if (key == GLFW_KEY_W && action == GLFW_PRESS && megaman.force[1] == 0.0f){
        megaman.force[1] = jump_force;
    }
    if (key == GLFW_KEY_A && action == GLFW_RELEASE){
        if(glfwGetKey(window,GLFW_KEY_D) == GLFW_PRESS){
            megaman.force[0] = speed;
            megaman.direction = 1;
        }
        else {
            keyReleased = true;
           // megaman.force[0] = 0.0f;
        }
    }
    if (key == GLFW_KEY_D && action == GLFW_PRESS){
        megaman.force[0] = speed;
        megaman.direction = 1;
        keyReleased = false;
    }
    if (key == GLFW_KEY_D && action == GLFW_RELEASE){
        if(glfwGetKey(window,GLFW_KEY_A) == GLFW_PRESS){
            megaman.force[0] = -speed;
            megaman.direction = -1;
        }
        else {
            keyReleased = true;
            //      megaman.force[0] = 0.0f;
        }
    }
    if (key == GLFW_KEY_H && action == GLFW_PRESS){
        if(tiles.animations[0].current_frame != tiles.animations->number_of_frames){
            tiles.animations[0].current_frame += 1;
        }else {
            tiles.animations[0].current_frame = 0;
        }
    }
}
unsigned int TextureFromFile(const char *filen, const char* directory, bool gamma, bool pixelated)
{
    char filename[256];
    snprintf(filename,sizeof(filename), "%s/%s", directory,filen);

    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = SOIL_load_image(filename, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLint format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        if(pixelated) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        }else{
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        }

        SOIL_free_image_data(data);
    }
    else
    {
        printf("Texture failed to load at path: %s\n", filename );
        SOIL_free_image_data(data);
    }

    return textureID;
}
