#version 450 core
out vec4 FragColor;

//in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
	vec4 texColor = texture2D(ourTexture, TexCoord);
	if(texColor.a < 1.0)
		discard;
	FragColor = texColor;
}

