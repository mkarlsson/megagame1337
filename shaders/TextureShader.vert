#version 450 core
layout (location = 0) in vec3 aPos;
//layout (location = 1) in vec3 aColor;
layout (location = 1) in vec2 aTexCoord;

//uniform int numberOfFrames;
//uniform int currentFrame;
//out vec3 ourColor;
out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform int direction;
void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    TexCoord = vec2(aTexCoord.x * direction, -aTexCoord.y);
  //  ourColor = aColor;
/*    if(aTexCoord.x != 0.0f){
        TexCoord = vec2((aTexCoord.x/numberOfFrames)*currentFrame, -aTexCoord.y);
    } else {
        TexCoord = vec2((1.0f/numberOfFrames)*(currentFrame-1), -aTexCoord.y);
    }*/
}
