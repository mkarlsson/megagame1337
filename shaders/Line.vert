#version 450 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
//layout(location = 1) in float collision;
//ayout(location = 1) in vec4 inccolour;

out vec4 outccolour;
// Values that stay constant for the whole mesh.
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 color;
void main(){
	// Output position of the vertex, in clip space : MVP * position
    gl_Position =  projection * view * model * vec4(vertexPosition_modelspace,1.0);
    outccolour = vec4(color,0.0f);
    /*if(collided){
            outccolour = vec4(1.0f, 0.0f, 0.0f, 0.0f);//inccolour;
    }else{
            outccolour = vec4(0.0f, 1.0f, 0.0f, 0.0f);//inccolour;
    }*/
}
